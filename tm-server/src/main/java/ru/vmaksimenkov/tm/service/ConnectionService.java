package ru.vmaksimenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class ConnectionService implements IConnectionService {

    private IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        Class.forName(propertyService.getJdbcDriver());
        @NotNull final String username = propertyService.getJdbcUsername();
        if (isEmpty(username)) throw new RuntimeException();
        @NotNull final String password = propertyService.getJdbcPassword();
        if (isEmpty(password)) throw new RuntimeException();
        @NotNull final String url = propertyService.getJdbcUrl();
        if (isEmpty(url)) throw new RuntimeException();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
