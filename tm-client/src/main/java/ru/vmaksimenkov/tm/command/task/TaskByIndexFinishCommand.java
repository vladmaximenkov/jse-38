package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIndexFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish task by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-finish-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        endpointLocator.getTaskEndpoint().finishTaskByIndex(endpointLocator.getSession(), TerminalUtil.nextNumber());
    }

}
